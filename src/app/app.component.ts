import { Component, OnInit } from '@angular/core';
import { Carro } from './Carro';
import { UsersService } from './services/users.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})



export class AppComponent implements OnInit {
  constructor(private serviceUser: UsersService) {}

  users = [];

  ngOnInit(): void {
    this.serviceUser.getUsuarios().subscribe(res => {

      console.log(res[5].name);

      this.users = res;
    });
  }
  
}