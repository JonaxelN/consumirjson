import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../modules/users.module';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { }

  getLlantasa(): number {
    return 4; 
  }

  getUsuarios(): Observable<User[]> {
    return this.http.get<User[]>("https://jsonplaceholder.typicode.com/users");
  }

  setUsuarios(user) {
    return this.http.post("https://jsonplaceholder.typicode.com/users", user);
  }
}
