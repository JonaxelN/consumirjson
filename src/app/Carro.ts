export class Carro {
    llantas: number;
    motor: boolean;
  
    constructor(private v_llantas:number, private v_motor: boolean ) {
      this.llantas = v_llantas;
      this.motor = v_motor;
    }

    prender() {
        console.log("Prendiendo");
    }
  }